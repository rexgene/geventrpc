package grpc

/**
 * Created with IntelliJ IDEA.
 * User: remote
 * Date: 12-12-11
 * Time: 上午11:06
 * To change this template use File | Settings | File Templates.
 */

import (
	"log"
	"net"
	"sync"
)

// RpcServer RpcServer
type RpcServer struct {
	sync.Mutex
	rpcBase
	listener net.Listener
	svcs     map[string]*Service
}

// NewRpcServer NewRpcServer
func NewRpcServer() (svr *RpcServer) {
	svr = &RpcServer{
		svcs: make(map[string]*Service),
	}
	svr.Init()
	return
}

// Bind addr
func (rpc *RpcServer) Bind(addr string) error {
	var err error
	rpc.Stop()
	err = rpc.resolveAddr(addr)
	if err != nil {
		return err
	}
	tcpAddr, ok := rpc.Addr.(*net.TCPAddr)
	if ok {
		rpc.listener, err = net.ListenTCP("tcp", tcpAddr)
		if err != nil {
			return err
		}
		log.Printf("[grpc]RpcServer tcp Bind: %s", addr)
	} else {
		unixAddr, _ := rpc.Addr.(*net.UnixAddr)
		rpc.listener, err = net.ListenUnix("unix", unixAddr)
		if err != nil {
			return err
		}
		log.Printf("[grpc]RpcServer unix Bind: %s", addr)
	}
	return nil
}

// Start Start
func (rpc *RpcServer) Start(wait bool) {
	if rpc.Started {
		return
	}
	rpc.rpcBase.Start()
	if wait {
		rpc.loop()
	} else {
		go rpc.loop()
	}
}

// Stop Stop
func (rpc *RpcServer) Stop() {
	if !rpc.Started {
		return
	}
	log.Printf("[grpc]RpcServer Stop!")
	rpc.rpcBase.Stop()
	rpc.close()
}

func (rpc *RpcServer) loop() {
	for {
		conn, err := rpc.listener.Accept()
		if err != nil {
			log.Printf("Error Accept:%s", err.Error())
			rpc.Stop()
			return
		}
		go rpc.newConn(conn)
	}
}

func (rpc *RpcServer) newConn(conn net.Conn) {
	uid := handshakeSvr(conn)
	if uid == "" {
		conn.Close()
		return
	}
	svc := NewService(rpc, uid, conn, nil, nil)
	rpc.Lock()
	rpc.svcs[uid] = svc
	rpc.Unlock()
	svc.Start()
}

//close rpc
func (rpc *RpcServer) close() {
	if rpc.listener != nil {
		rpc.listener.Close()
		rpc.listener = nil
	}
}

// StopService StopService
func (rpc *RpcServer) StopService(svc *Service) {
	rpc.Lock()
	defer rpc.Unlock()
	delete(rpc.svcs, svc.UID)
}
