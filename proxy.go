/**
 * Created with IntelliJ IDEA.
 * User: see
 * Date: 13-05-24
 * Time: 上午10:410
 * proxy
 */

package grpc

import (
	"reflect"
)

// Proxy Proxy
type Proxy interface {
	Init(svc *Service, id string)
	GetService() *Service
	Call(name string, args []interface{}, noResult, proxy bool, timeout int64) (interface{}, error)
}

// Proxyer Proxyer
type Proxyer struct {
	Svc *Service
	id  string
}

// ProxyType ProxyType
var ProxyType = reflect.TypeOf(&Proxyer{})

// Init Init
func (p *Proxyer) Init(svc *Service, id string) {
	p.Svc = svc
	p.id = id
}

// GetService GetService
func (p *Proxyer) GetService() *Service {
	return p.Svc
}

// Call Call
func (p *Proxyer) Call(name string, args []interface{},
	noResult, proxy bool, timeout int64) (rs interface{}, err error) {
	rs, err = p.Svc.Call(p.id, name, args, nil, noResult, proxy, timeout)
	return
}
