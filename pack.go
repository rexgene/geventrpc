package grpc

/**
 * Created with IntelliJ IDEA.
 * User: remote
 * Date: 12-12-18
 * Time: 上午11:43
 * To change this template use File | Settings | File Templates.
 */

import (
	//	msgpack "github.com/msgpack/msgpack-go"
	//	msgpack "github.com/ugorji/go-msgpack"
	"encoding/json"
	"reflect"

	"github.com/ugorji/go/codec"
)

var (
	// DefaultPacker DefaultPacker
	DefaultPacker = &MsgPacker{}
	// MsgPackH MsgPackH
	MsgPackH = &codec.MsgpackHandle{}
	// MsgHandle MsgHandle
	MsgHandle = &codec.MsgpackHandle{}
)

func init() {
	MsgHandle.RawToString = true
	MsgPackH.RawToString = true
	MsgPackH.WriteExt = true
}

// PackObjExt PackObjExt
type PackObjExt interface {
	// WriteExt converts a value to a []byte.
	//
	// Note: v *may* be a pointer to the extension type, if the extension type was a struct or array.
	WriteExt(v interface{}) []byte

	// ReadExt updates a value from a []byte.
	ReadExt(dst interface{}, src []byte)
}

// Packer Packer
type Packer interface {
	Pack(value interface{}) (data []byte, err error)
	Unpack(data []byte) (value interface{})
	PackExtReg(rt reflect.Type, tag uint64, ext PackObjExt) (err error)
}

// MsgPacker MsgPacker
type MsgPacker struct {
}

// Pack Pack
func (mp *MsgPacker) Pack(v interface{}) (data []byte, err error) {
	//	data, err = msgpack.Marshal(v)
	err = codec.NewEncoderBytes(&data, MsgPackH).Encode(v)
	//	log.Println("msgpack:", v, data, err)
	return
}

// Unpack Unpack
func (mp *MsgPacker) Unpack(data []byte) (v interface{}) {
	//	msgpack.Unmarshal(data, &v, nil)
	codec.NewDecoderBytes(data, MsgPackH).Decode(&v)
	//	log.Println("msgUnpack:", v, data)
	return
}

// PackExtReg PackExtReg
func (mp *MsgPacker) PackExtReg(rt reflect.Type, tag uint64, ext PackObjExt) (err error) {
	if ext == nil {
		ext = CodecExt{}
	}
	return MsgPackH.SetBytesExt(rt, tag, ext)
}

// CodecExt CodecExt
type CodecExt struct {
}

// WriteExt WriteExt
func (ce CodecExt) WriteExt(v interface{}) (data []byte) {
	err := codec.NewEncoderBytes(&data, MsgHandle).Encode(v)
	if err != nil {
		return nil
	}
	return
}

// ReadExt ReadExt
func (ce CodecExt) ReadExt(dst interface{}, src []byte) {
	codec.NewDecoderBytes(src, MsgHandle).Decode(dst)
}

// JSONExt JSONExt
type JSONExt struct {
}

// WriteExt WriteExt
func (ce JSONExt) WriteExt(v interface{}) []byte {
	//log.Println("***WriteExt****", v, "*************")
	rs, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}
	return rs
}

// ReadExt ReadExt
func (ce JSONExt) ReadExt(dst interface{}, src []byte) {
	json.Unmarshal(src, dst)
	//log.Println("***ReadExt****", dst, "*************")
}
