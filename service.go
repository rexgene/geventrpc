package grpc

/**
 * Created with IntelliJ IDEA.
 * User: remote
 * Date: 12-12-18
 * Time: 上午11:16
 * To change this template use File | Settings | File Templates.
 */

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
	"time"
	//	"strconv"
	//	"net/rpc"
	"compress/zlib"
	"reflect"
	//"sync/atomic"
	//"unsafe"
	"sync"
	"sync/atomic"
	"unsafe"
)

const (
	// buffLEN buffLEN
	buffLEN = 1024
	// uidLEN uidLEN
	uidLEN = 32
	// chanBUFFLEN chanBUFFLEN
	chanBUFFLEN = 10
	// magicNUM magicNUM
	magicNUM = 0x0CEE0905 // Used to identify data header
)

var (
	// nodeKW nodeKW
	nodeKW        = make(map[interface{}]interface{})
	valueIndexint = reflect.ValueOf(indexint(1))
	// ServiceMethod ServiceMethod
	ServiceMethod map[string]*methodType
)

type indexint uint32

// Service Service
type Service struct {
	UID    string
	Stoped bool
	sock   net.Conn
	rpc    RPC
	chSend chan []byte
	chResp chan *resper
	//chWait chan *waiter
	chnoWait  chan indexint
	lkWait    sync.Mutex
	r, w      []byte
	export    *Export
	index     indexint
	heartTime int64
	waits     map[indexint]*waiter
	rfunc     ReconnectFunc
	userdata  interface{}
}

type waiter struct {
	i indexint
	c chan interface{}
}
type resper struct {
	i   indexint
	rs  interface{}
	err error
}

// NewService NewService
func NewService(rpc RPC, uid string, conn net.Conn, rfunc ReconnectFunc, userdata interface{}) (svc *Service) {
	svc = &Service{UID: uid, Stoped: true,
		sock:     conn,
		rpc:      rpc,
		r:        make([]byte, buffLEN),
		w:        make([]byte, buffLEN),
		waits:    make(map[indexint]*waiter),
		rfunc:    rfunc,
		userdata: userdata,
	}
	svc.chSend = make(chan []byte, chanBUFFLEN)
	svc.chResp = make(chan *resper, chanBUFFLEN)
	//svc.chWait = make(chan *waiter, CHAN_BUFF_LEN)
	svc.chnoWait = make(chan indexint, chanBUFFLEN)
	svc.export = &Export{v: reflect.ValueOf(svc), t: reflect.TypeOf(svc)}
	svc.export.method = ServiceMethod
	return
}

func handshakeSvr(sock net.Conn) string {
	buf := make([]byte, uidLEN)
	n, err := sock.Read(buf)
	if err != nil || n != uidLEN {
		return ""
	}
	return string(buf)
}

func handshakeCli(sock net.Conn, uid string) string {
	if len(uid) != uidLEN {
		return ""
	}
	buf := []byte(uid)
	n, err := sock.Write(buf)
	if err != nil || n != uidLEN {
		return ""
	}
	return uid
}

func (svc *Service) heartbeat() {
	//log.Printf("[grpc]heartbeat(%s) start:%s\n", HEARTBEAT_TIME / time.Second, svc.sock.RemoteAddr())
	svc.heartTime = time.Now().Unix()
	checkTimes := int64(reconnectTIMEOUT) * int64(heartbeatTIME)
	for !svc.Stoped {
		//此处有bug,不要用Write成功当作 心跳成功，因为Write(send)缓冲区很大
		//另外,这个是调用svc.send 里面用chan送到发送队列,基本上不可能用来检测掉线
		// TODO FIX
		err := svc.send(rtHEARTBEAT)
		if err != nil {
			break
		}
		time.Sleep(heartbeatTIME)
		//check time out
		curTime := time.Now().Unix()
		if svc.heartTime+checkTimes < curTime {
			log.Printf("[grpc]****WARNING*** heartbeat timeout:%s\n", svc.sock.RemoteAddr())
			svc.sock.Close()
			break
		}
		svc.heartTime = curTime
	}
}

// Start Start
func (svc *Service) Start() {
	if !svc.Stoped {
		return
	}
	//	log.Printf("[grpc]Service(%s) Start", svc.Uid)
	svc.Stoped = false
	go svc.goRecv()
	go svc.goSend()
	go svc.goWait()
	go svc.heartbeat()
}

// Stop Stop
func (svc *Service) Stop() {
	if svc.Stoped {
		return
	}
	svc.Stoped = true
	//log.Printf("[grpc]Service(%s) Stop", svc.Uid)
	svc.uninit()
}

func (svc *Service) uninit() {
	svc.closeSock()
	close(svc.chnoWait)
	close(svc.chResp)
	//close(svc.chWait)
	close(svc.chSend)
	svc.rpc.StopService(svc)
}

func (svc *Service) closeSock() {
	svc.Call("", "remote_stop", nil, nil, true, false, 0)
	time.Sleep(10 * time.Millisecond)
	svc.sock.Close()
}

// func (svc *Service) Remote_stop() error {
// 	svc.sock.Close()
// 	svc.Stop()
// 	return nil
// }

func (svc *Service) _handleRecvErr(tag string, err error) {
	if svc.Stoped {
		return
	}

	if string(err.Error()) == "EOF" {
		return
	}

	log.Printf("[grpc]goRecv read %s error:%s", tag, err.Error())
}

func (svc *Service) goRecv() {
	var hl uint32
	var mb uint32
	var db []byte
	var err error
	hb := make([]byte, 8)

	defer func() {
		if r := recover(); r != nil {
			PrintRecover(r)
			if svc.Stoped {
				return
			}
			log.Printf("[grpc]goRecv error:%s", r)
		}
		log.Println("gevent be stop...")
		svc.Stop()
		log.Println("gevent be stop ok")
		if nil != svc.rfunc {
			log.Println("gevent reconnect start...")
			go svc.rfunc(svc.rpc.(*RpcClient), svc.UID, svc.userdata)
		}
	}()

	for !svc.Stoped {
		err = svc.read(hb)
		if err != nil {
			svc._handleRecvErr("header", err)
			return
		}

		mb = binary.LittleEndian.Uint32(hb)
		if mb != magicNUM {
			// shut down this connection if receive illegal header
			log.Println("[grpc]goRecv receive illegal header")
			return
		}

		hl = binary.LittleEndian.Uint32(hb[4:])
		db = make([]byte, hl)
		err = svc.read(db)
		if err != nil {
			svc._handleRecvErr("data", err)
			return
		}
		v := svc.rpc.Unpack(db).([]interface{})
		//log.Printf("Service.goRecv :%s", v)
		go svc.handle(v)
	}
}

func (svc *Service) goSend() {

	var i, n int
	var l uint32
	var err error
	hb := make([]byte, 8)
	// defer svc.Stop()
	for !svc.Stoped {
		d, ok := <-svc.chSend
		if !ok {
			break
		}
		//log.Printf("Service.goSend :%d", len(d))
		l = uint32(len(d))

		binary.LittleEndian.PutUint32(hb, magicNUM)
		binary.LittleEndian.PutUint32(hb[4:], l)
		n, _ = svc.sock.Write(hb)
		i = 0
		for {
			n, err = svc.sock.Write(d[i:l])
			//			log.Printf("[grpc]goSend data:%d, %s", n, d[i:l])
			if err != nil {
				if svc.Stoped {
					return
				}
				log.Printf("[grpc]goSend error:%s", err.Error())
				return
			}
			i += n
			if int(l) == i {
				break
			}
		}
	}
}

func (svc *Service) getWait(i indexint) (*waiter, bool) {
	svc.lkWait.Lock()
	w, ok := svc.waits[i]
	svc.lkWait.Unlock()
	return w, ok
}

func (svc *Service) goWait() {
	defer func() {
		if r := recover(); r != nil {
			PrintRecover(r)
			if svc.Stoped {
				return
			}
			log.Printf("goWait error:%s", r)
		}
		// svc.Stop()
	}()
	for !svc.Stoped {
		// first get all chWait's msg, then do other's
		select {
		case rs, ok := <-svc.chResp:
			//if len(svc.chResp) >= 5 {
			//	log.Printf("goWait, svc.chResp len:%d", len(svc.chResp))
			//}
			if !ok {
				break
			}

			if rs == nil {
				continue
			}
			w, ok := svc.getWait(rs.i)
			if ok {
				svc.lkWait.Lock()
				delete(svc.waits, w.i)
				svc.lkWait.Unlock()
				//log.Printf("<- chResp:%s, %d, %s", rs, w.i, svc.Uid)
				//delete(svc.waits, w.i)
				if rs.err != nil {
					w.c <- rs.err
				} else {
					w.c <- rs.rs
				}
				close(w.c)
			} else {
				log.Printf("goWait Error: wait(%d) no found**********%s", rs.i, svc.UID)
			}
		case i, ok := <-svc.chnoWait:
			if !ok {
				break
			}
			//log.Printf("<- chnoWait:%s, %s", i, svc.Uid)
			w, ok := svc.getWait(i)
			if ok {
				svc.lkWait.Lock()
				delete(svc.waits, i)
				svc.lkWait.Unlock()
				close(w.c)
			}
		}
	}
	svc.lkWait.Lock()
	defer svc.lkWait.Unlock()
	for key, w := range svc.waits {
		delete(svc.waits, key)
		w.c <- errors.New("service close")
		close(w.c)
	}
}

func (svc *Service) handle(v []interface{}) {
	defer func() {
		if r := recover(); r != nil {
			PrintRecover(r)
			log.Printf("[ERROR]handle:%s", r)
		}
	}()

	var dtype, index uint
	var objID, name string
	dtype = iftoui(v[0])
	rt := dtype & rtMARK
	if rt == rtHEARTBEAT || len(v) == 1 {
		return //heart beat
	}
	switch v[1].(type) {
	case string:
		objID = v[1].(string)
	case int8, int16, int32:
		objID = ""
	}

	//log.Printf("[grpc]handle rt:%d, v:%s", rt, v)
	switch rt {
	case rtREQUEST:
		//log.Println("~~~~", v)
		index = iftoui(v[2])
		name = v[3].(string)
		//argkw = v[4].([]byte)
		svc.handleRequest(dtype, index, objID, name, v[4].([]byte))
	case rtRESPONSE:
		index = iftoui(v[1])
		svc.handleResponse(dtype, index, v[2].([]byte))
	case rtEXCEPTION:
		index = iftoui(v[1])
		name = v[2].(string)
		svc.handleException(dtype, index, name)
	}
}

func (svc *Service) handleRequest(dtype, index uint, objID, name string, argkw []byte) {
	//log.Printf("[grpc]handleRequest v:%s, %s, %s", obj_id, name, argkw)
	var err error
	export := svc.GetExport(objID)
	if export == nil {
		svc.send(rtEXCEPTION, index, fmt.Sprintf("export(%s) no found", objID))
		return
	}
	mtype, ok := export.method[strings.Title(name)]
	if !ok {
		svc.send(rtEXCEPTION, index, fmt.Sprintf("export(%s) func(%s) no found", objID, name))
		return
	}

	var dargkw []byte
	if dtype&dtZIP == dtZIP {
		var akbuf bytes.Buffer
		b1 := bytes.NewBuffer(argkw)
		r, err := zlib.NewReader(b1)
		if err != nil {
			svc.send(rtEXCEPTION, index, fmt.Sprintf("export(%s) zlib error:(%s)", objID, err.Error()))
			return
		}
		io.Copy(&akbuf, r)
		dargkw = akbuf.Bytes()
	} else {
		dargkw = argkw
	}

	var d []interface{}
	switch x := svc.rpc.Unpack(dargkw); y := x.(type) {
	case []interface{}:
		d = y
	default:
		svc.send(rtEXCEPTION, index, fmt.Sprintf("export(%s) Unpack error:%s", objID, x))
		return
	}
	args, ok := d[0].([]interface{})
	kw, ok := d[1].(map[interface{}]interface{})
	if dtype&dtPROXY == dtPROXY {
		args[0], err = svc.GetProxys(args[0])
		if err != nil {
			svc.send(rtEXCEPTION, index, fmt.Sprintf("export(%s) Proxy error:%s", objID, err.Error()))
			return
		}
	}

	rs, err := svc.callExport(export, mtype, args, kw)
	if dtype&stNoRESULT == stNoRESULT {
		return
	}
	if err != nil {
		svc.send(rtEXCEPTION, index, fmt.Sprintf("error:%s", err.Error()))
		//log.Printf("handle_request error:%s", err.Error())
		return
	}

	var b1 []byte
	b1, err = svc.rpc.Pack(rs)
	if err != nil {
		svc.send(rtEXCEPTION, index, fmt.Sprintf("export(%s) Pack result error:%s", objID, err.Error()))
		return
	}
	svc.send(rtRESPONSE, index, b1)
}

func (svc *Service) handleResponse(dtype, index uint, argkw []byte) {
	resp := &resper{i: indexint(index), rs: svc.rpc.Unpack(argkw)}
	svc.chResp <- resp
}

func (svc *Service) handleException(dtype, index uint, e string) {
	resp := &resper{i: indexint(index), err: errors.New(e)}
	svc.chResp <- resp
}

func (svc *Service) send(data ...interface{}) (err error) {
	defer func() {
		if r := recover(); r != nil {
			PrintRecover(r)
			log.Printf("[ERROR]recover send:%s", r)
			err = errors.New("close")
		}
	}()

	//log.Printf("send:%s", data)
	d, err := svc.rpc.Pack(data)
	if err != nil {
		log.Printf("send error:%s", err.Error())
		return
	}
	svc.chSend <- d
	return nil
}

func (svc *Service) read(buf []byte) (err error) {
	c := len(buf)
	i := 0
	n := 0
	//svc.sock.SetReadDeadline(time.Now().Add(10 * time.Second))  // test
	for c > 0 {
		n, err = svc.sock.Read(buf[i:])
		if err != nil {
			return err
		}
		i += n
		c -= n
	}
	return nil
}

func (svc *Service) nextIndex() (rs indexint) {
	p := (*uint32)(unsafe.Pointer(&svc.index))
	if svc.index >= 4294900000 {
		//if valueIndexint.OverflowUint(uint64(svc.index) + 1) {
		rs = indexint(atomic.SwapUint32(p, 1))
		//svc.index = 1
	} else {
		rs = indexint(atomic.AddUint32(p, 1))
		//svc.index = svc.index + 1
	}
	//rs = svc.index
	//log.Printf("~~~~~~%d", rs)
	return
}

func (svc *Service) preReadResponse(index indexint) chan interface{} {
	c := make(chan interface{}, 0)
	w := &waiter{
		c: c,
		i: index,
	}
	svc.lkWait.Lock()
	svc.waits[w.i] = w
	svc.lkWait.Unlock()
	return c
}

func (svc *Service) readResponse(index indexint, c chan interface{}, timeout int64) (rs interface{}, err error) {
	defer func() {
		if r := recover(); r != nil {
			PrintRecover(r)
			rs, err = nil, r.(error)
		}
	}()

	select {
	case r, ok := <-c:
		if !ok {
			return nil, errors.New("readResponse error")
		}
		err, ok = r.(error)
		if !ok {
			rs = r
			err = nil
		}
		return
	case <-time.After(time.Duration(timeout)):
		svc.chnoWait <- index
		err = fmt.Errorf("TimeOut(%d)", timeout)
	}
	return
}

// Call Call
func (svc *Service) Call(objID, name string, args []interface{}, kw map[interface{}]interface{},
	noResult, proxy bool, timeout int64) (rs interface{}, err error) {
	var d []byte
	var dtype indexint
	dtype = rtREQUEST
	if proxy {
		dtype |= dtPROXY
	}
	if kw == nil {
		kw = nodeKW
	}

	d, err = svc.rpc.Pack([]interface{}{args, kw})
	if err != nil {
		log.Printf("call error:%s", err.Error())
		return
	}
	if len(d) >= zipLENGTH {
		zbuf := bytes.NewBuffer(nil)
		dtype |= dtZIP
		w := zlib.NewWriter(zbuf)
		w.Write(d)
		w.Close()
		d = zbuf.Bytes()
	}
	if noResult {
		dtype |= stNoRESULT
	}
	index := svc.nextIndex()
	//log.Printf("**********%d======", index)
	var c chan interface{}
	if !noResult {
		//if net very fast, sometimes, RespData arrive but not begin readResponse @_@.
		//so, preReadResponse before svc.send
		c = svc.preReadResponse(index)
	}
	err = svc.send(dtype, objID, index, name, d)
	if err != nil {
		if !noResult {
			svc.chnoWait <- index // del preReadResponse
		}
		return
	}
	if noResult {
		return nil, nil
	}
	if timeout <= 0 {
		timeout = int64(defaultTIMEOUT)
	}
	rs, err = svc.readResponse(index, c, timeout)
	return
}

// GetExport GetExport
func (svc *Service) GetExport(objID string) *Export {
	if objID == "" {
		return svc.export
	}
	return svc.rpc.GetExport(objID)
}

// GetProxys GetProxys
func (svc *Service) GetProxys(ids interface{}) (rs interface{}, err error) {
	id, ok := ids.(string)
	if ok {
		o, err := svc.GetProxy(id)
		if err != nil {
			return nil, err
		}
		return o.Interface(), nil
	}
	_ids, ok := ids.([]interface{})
	if !ok {
		return nil, errors.New("GetProxys error")
	}

	objs := make([]interface{}, len(_ids))
	for i, v := range _ids {
		id = v.(string)
		o, err := svc.GetProxy(id)
		if err != nil {
			return nil, err
		}
		objs[i] = o.Interface()
	}
	return objs, nil
}

// GetProxy GetProxy
func (svc *Service) GetProxy(id string) (rs reflect.Value, err error) {
	rs, err = svc.rpc.NewProxy(id)
	if err != nil {
		return
	}
	rs.Interface().(Proxy).Init(svc, id)
	return
}

func callMethod(export *Export, mtype *methodType, args []interface{},
	kw map[interface{}]interface{}) (rs []reflect.Value, err error) {
	defer func() {
		if r := recover(); r != nil {
			PrintRecover(r)
			rs, err = nil, recoverToError(r)
		}
	}()
	var it reflect.Type
	function := mtype.method.Func
	//log.Printf("[grpc]Service.call:(%d)%s, %s", mtype.mtype.NumIn(), args, kw)
	l := mtype.mtype.NumIn()
	params := make([]reflect.Value, l)
	params[0] = export.v
	for i := 0; i < len(args); i++ {
		it = mtype.mtype.In(i + 1)

		if reflect.ValueOf(args[i]).Kind() != reflect.Struct {
			// log.Println("----------", reflect.ValueOf(args[i]).Type(), it)
			params[i+1] = reflect.ValueOf(args[i]).Convert(it)

		} else {

			if reflect.ValueOf(args[i]).Type() == it {
				// log.Println("!!!!!!!!", reflect.ValueOf(args[i]).Type(), it)
				params[i+1] = reflect.ValueOf(args[i]).Convert(it)
			} else {
				// log.Println("##########", reflect.ValueOf(args[i]).Type(), it)

				val := reflect.ValueOf(args[i])
				vp := reflect.New(val.Type())
				vp.Elem().Set(val)

				params[i+1] = vp
			}
		}

	}
	if (l - 1 - len(args)) > 0 {
		params[l-1] = reflect.ValueOf(kw)
	}
	rs = function.Call(params)
	lout := mtype.mtype.NumOut()
	if lout > 0 && mtype.mtype.Out(lout-1).Implements(reflect.TypeOf((*error)(nil)).Elem()) {
		v := rs[lout-1].Interface()
		if v != nil {
			err = v.(error)
		}
		return rs[0 : lout-1], err
	}
	return rs, nil

}

func (svc *Service) callExport(export *Export, mtype *methodType, args []interface{},
	kw map[interface{}]interface{}) (rs interface{}, err error) {
	defer func() {
		if r := recover(); r != nil {
			PrintRecover(r)
			rs, err = nil, recoverToError(r)
		}
	}()

	var returnValues []reflect.Value
	returnValues, err = callMethod(export, mtype, args, kw)
	if err != nil {
		return
	}

	rl := len(returnValues)
	//if rl > 0 {
	//	errInter := returnValues[rl-1].Interface()
	//	if errInter != nil {
	//		err, ok := errInter.(error)
	//		if !ok {
	//			err = errors.New(errInter.(string))
	//		}
	//		return nil, err
	//	}
	//}
	switch rl {
	case 0:
		return nil, nil
	case 1:
		return returnValues[0].Interface(), err
		//case 2:
		//	return returnValues[0].Interface(), err
	}
	ret := make([]interface{}, rl)
	for i := 0; i < rl; i++ {
		ret[i] = returnValues[i].Interface()
	}
	return ret, err
}

func init() {
	ServiceMethod = suitableMethods(reflect.TypeOf(new(Service)), false)
}
