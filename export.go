package grpc

/**
 * Created with IntelliJ IDEA.
 * User: remote
 * Date: 12-12-18
 * Time: 上午10:42
 * To change this template use File | Settings | File Templates.
 */

import (
	"reflect"
	"sync"
)

type methodType struct {
	//	sync.Mutex // protects counters
	method    reflect.Method
	mtype     reflect.Type
	numReturn int
	numCalls  uint
}

// Export Export
type Export struct {
	v      reflect.Value
	t      reflect.Type
	method map[string]*methodType
}

// NoFoundHandle NoFoundHandle
type NoFoundHandle func(name string) bool

type exportHandler struct {
	sync.Mutex
	exports  map[string]*Export
	noFoundH NoFoundHandle
}

// GetInterface GetInterface
func (e *Export) GetInterface() interface{} {
	return e.v.Interface()
}

// suitableMethods returns suitable Rpc methods of typ, it will report
// error using log if reportErr is true.
func suitableMethods(typ reflect.Type, reportErr bool) (methods map[string]*methodType) {
	methods = make(map[string]*methodType)
	//printf("suitableMethods:%s", typ.Name())
	for m := 0; m < typ.NumMethod(); m++ {
		method := typ.Method(m)
		mtype := method.Type
		mname := method.Name
		//printf("suitableMethods:%s", mname)
		// Method must be exported.
		//		if false && method.PkgPath != "" {
		//			continue
		//		}
		numReturn := mtype.NumOut()
		methods[mname] = &methodType{method: method,
			mtype:     mtype,
			numReturn: numReturn,
		}
	}
	return methods
}

func newExportHandler() (hd *exportHandler) {
	hd = &exportHandler{
		exports: make(map[string]*Export),
	}
	return hd
}

func (hd *exportHandler) RegNoFoundHandle(handle NoFoundHandle) {
	hd.noFoundH = handle
}

func (hd *exportHandler) Register(name string, export interface{}) {
	e := &Export{v: reflect.ValueOf(export), t: reflect.TypeOf(export)}
	e.method = suitableMethods(e.t, true)
	hd.Lock()
	hd.exports[name] = e
	hd.Unlock()
}

func (hd *exportHandler) UnRegister(name string) {
	hd.Lock()
	delete(hd.exports, name)
	hd.Unlock()
}

func (hd *exportHandler) GetExport(name string) *Export {
	hd.Lock()
	exp, ok := hd.exports[name]
	hd.Unlock()
	if ok {
		return exp
	}
	if hd.noFoundH != nil && hd.noFoundH(name) {
		hd.Lock()
		rs := hd.exports[name]
		hd.Unlock()
		return rs
	}
	return nil
}
