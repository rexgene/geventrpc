package grpc

/**
 * Created with IntelliJ IDEA.
 * User: remote
 * Date: 12-12-11
 * Time: 上午11:06
 * To change this template use File | Settings | File Templates.
 */

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"net"
	"reflect"
	"time"
)

// RpcClient RpcClient
type RpcClient struct {
	rpcBase
	sock net.Conn
	svc  *Service
}

// ReconnectFunc ReconnectFunc
type ReconnectFunc func(c *RpcClient, ouid string, userdata interface{})

// NewRpcClient NewRpcClient
func NewRpcClient() (client *RpcClient) {
	client = &RpcClient{}
	client.Init()
	return
}

// GenUUID GenUUID
func GenUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := rand.Read(uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	// TODO: verify the two lines implement RFC 4122 correctly
	uuid[8] = 0x80 // variant bits see page 5
	uuid[4] = 0x40 // version 4 Pseudo Random, see page 7

	return hex.EncodeToString(uuid), nil
}

// Connect to addr
// rfunc 是运行在一个协程中，在掉线的时候调用
func (rpc *RpcClient) Connect(addr string, timeout time.Duration, rfunc ReconnectFunc, oUID string, userdata interface{}) (err error) {
	rpc.Stop()
	err = rpc.resolveAddr(addr)
	if err != nil {
		return
	}
	rpc.sock, err = net.DialTimeout(rpc.Net, addr, timeout)
	if err != nil {
		return
	}

	//	tcp_addr, ok := rpc.Addr.(*net.TCPAddr)
	//	if ok {
	//		rpc.sock, err = net.DialTCP("tcp", nil, tcp_addr)
	//		net.DialTimeout("tcp", tcp_addr, timeout)
	//		if err != nil {
	//			return
	//		}
	//	} else {
	//		unix_addr, _ := rpc.Addr.(*net.UnixAddr)
	//		rpc.sock, err = net.DialUnix("unix", nil, unix_addr)
	//		if err != nil {
	//			return
	//		}
	//	}

	uid, _ := GenUUID()

	if len(oUID) == uidLEN {
		uid = oUID
	}

	uid = handshakeCli(rpc.sock, uid)
	if uid == "" {
		rpc.sock.Close()
		rpc.sock = nil
		err = fmt.Errorf("handsshake error:%s", uid)
		return
	}

	rpc.svc = NewService(rpc, uid, rpc.sock, rfunc, userdata)

	if len(oUID) == uidLEN {
		rpc.Start()
	}

	return
}

//close rpc
func (rpc *RpcClient) close() {
	if rpc.sock != nil {
		rpc.sock.Close()
		rpc.sock = nil
		rpc.svc = nil
	}
}

// IsStop IsStop
func (rpc *RpcClient) IsStop() bool {
	if rpc.svc == nil {
		return true
	}

	return false
}

// Start Start
func (rpc *RpcClient) Start() {
	if rpc.Started {
		return
	}
	rpc.rpcBase.Start()
	rpc.svc.Start()
}

// Stop Stop
func (rpc *RpcClient) Stop() {
	if !rpc.Started {
		return
	}
	rpc.rpcBase.Stop()
	rpc.svc.Stop()
	rpc.svc = nil
	rpc.close()
}

// Call Call
func (rpc *RpcClient) Call(objID, name string, args []interface{}, kw map[interface{}]interface{},
	noResult, proxy bool, timeout int64) (rs interface{}, err error) {
	return rpc.svc.Call(objID, name, args, kw, noResult, proxy, timeout)
}

// GetProxy GetProxy
func (rpc *RpcClient) GetProxy(id string, p Proxy) (rs Proxy) {
	v := reflect.ValueOf(p)
	if v.IsNil() {
		rs = &Proxyer{}
	} else {
		rs = p
	}
	rs.Init(rpc.svc, id)
	//printf("GetProxy:%s, %s, %s", id, rs)
	return rs
}

// InitProxy InitProxy
func (rpc *RpcClient) InitProxy(id string, p Proxy) {
	p.Init(rpc.svc, id)
}

// StopService StopService
func (rpc *RpcClient) StopService(svc *Service) {
	rpc.Stop()
}
