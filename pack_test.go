package grpc

import (
	"encoding/json"
	"log"
	"reflect"
	"testing"
)

type Pack1Obj struct {
	A string
	B int
	C bool
}

type PackPExt struct {
}

func (ppe PackPExt) WriteExt(v interface{}) []byte {
	log.Println("*PackPExt**WriteExt****", v, "*************")
	rs, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}
	return rs
}

func (ppe PackPExt) ReadExt(dst interface{}, src []byte) {
	json.Unmarshal(src, dst)
	log.Println("*PackPExt**ReadExt****", dst, "*************")
}

func _reg(pack *MsgPacker, t reflect.Type, tag uint64) {
	err := pack.PackExtReg(t, tag, JSONExt{})
	if err != nil {
		log.Println("~_reg PackExtReg~~~~error:", err)
	}
}

func _regP(pack *MsgPacker, t reflect.Type, tag uint64) {
	err := pack.PackExtReg(t, tag, PackPExt{})
	if err != nil {
		log.Println("~_regP PackExtReg~~~~error:", err)
	}
}

func TestMsgPacker(t *testing.T) {
	obj := Pack1Obj{A: "name", B: 123, C: true}
	pack := &MsgPacker{}
	_reg(pack, reflect.TypeOf(obj), 1)
	_regP(pack, reflect.TypeOf(&obj), 2)
	data, err := pack.Pack([]interface{}{&obj})
	if err != nil {
		t.Error(err)
	}
	log.Println("pack data:", data)
	v := pack.Unpack(data)
	log.Println("unpack value:", v)
	obj1 := v.([]interface{})[0]
	log.Println("unpack obj:", obj1, reflect.TypeOf(obj1))
}
