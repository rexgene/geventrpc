package grpc

/**
 * Created with IntelliJ IDEA.
 * User: remote
 * Date: 12-12-11
 * Time: 上午11:06
 * To change this template use File | Settings | File Templates.
 */

import (
	"errors"
	"net"
	"reflect"
	"time"
)

const (
	// rtREQUEST rpc数据类型
	rtREQUEST = 1 << iota
	// rtRESPONSE rtRESPONSE
	rtRESPONSE
	// rtHEARTBEAT rtHEARTBEAT
	rtHEARTBEAT
	// rtEXCEPTION rtEXCEPTION
	rtEXCEPTION
	// stNoRESULT rpc处理类型
	stNoRESULT = 1 << 5
	// stNoMSG stNoMSG
	stNoMSG = 1 << 6
	// dtPICKLE rpc参数类型
	dtPICKLE = 1 << 7 //默认用msgpack
	// dtZIP dtZIP
	dtZIP = 1 << 8
	// dtPROXY dtPROXY
	dtPROXY = 1 << 9 //标示传递的第1个参数是obj, 需要转换成proxy
	// rtMARK rpc数据类型 mark
	rtMARK = stNoRESULT - 1
	// callTIMEORUT callTIMEORUT
	callTIMEORUT = 120
	// zipLENGTH zipLENGTH
	zipLENGTH = 1024 * 2 //if argkw > Nk, use zlib to compress
	// zipLEVEL zipLEVEL
	zipLEVEL = 3
)

var (
	// debug debug
	debug = true
	// defaultTIMEOUT defaultTIMEOUT
	defaultTIMEOUT = 30 * time.Second
	// heartbeatTIME heartbeatTIME
	heartbeatTIME = 60 * time.Second //heartbeat, if disconnect time > (HEARTBEAT_TIME + RECONNECT_TIMEOUT), connect lost
	// reconnectTIMEOUT reconnectTIMEOUT
	reconnectTIMEOUT = 3 //wait reconnect time, zero will disable reconnect wait
)

// RPC RPC
type RPC interface {
	Pack(value interface{}) (data []byte, err error)
	Unpack(data []byte) (value interface{})
	PackExtReg(rt reflect.Type, tag uint64, ext PackObjExt) (err error)
	GetExport(objID string) *Export
	RegProxy(id string, ptype reflect.Type) error
	NewProxy(id string) (rs reflect.Value, err error)
	StopService(svc *Service)
}

type rpcBase struct {
	Started bool
	Net     string
	Addr    net.Addr
	SAddr   string
	Host    string
	Port    int
	exports *exportHandler
	Packer  Packer
	proxys  map[string]reflect.Type
}

func resolveAddr(addr string) (network string, rs net.Addr, err error) {
	network = "tcp"
	rs, err = net.ResolveTCPAddr(network, addr)
	if err != nil {
		network = "unix"
		rs, err = net.ResolveUnixAddr(network, addr)
	}
	return
}

func (rpc *rpcBase) Init() {
	rpc.Packer = DefaultPacker
	rpc.exports = newExportHandler()
	rpc.proxys = make(map[string]reflect.Type)
}

func (rpc *rpcBase) Pack(value interface{}) (data []byte, err error) {
	return rpc.Packer.Pack(value)
}

func (rpc *rpcBase) Unpack(data []byte) (value interface{}) {
	return rpc.Packer.Unpack(data)
}

func (rpc *rpcBase) PackExtReg(rt reflect.Type, tag uint64, ext PackObjExt) (err error) {
	return rpc.Packer.PackExtReg(rt, tag, ext)
}

func (rpc *rpcBase) resolveAddr(addr string) (err error) {
	rpc.Net, rpc.Addr, err = resolveAddr(addr)
	if err != nil {
		return err
	}
	rpc.SAddr = addr
	return err
}

func (rpc *rpcBase) Start() {
	if rpc.Started {
		return
	}
	rpc.Started = true
}

func (rpc *rpcBase) Stop() {
	if !rpc.Started {
		return
	}
	rpc.Started = false
}

func (rpc *rpcBase) Register(name string, export interface{}) {
	rpc.exports.Register(name, export)
}

func (rpc *rpcBase) UnRegister(name string) {
	rpc.exports.UnRegister(name)
}

func (rpc *rpcBase) GetExport(objID string) *Export {
	return rpc.exports.GetExport(objID)
}

func (rpc *rpcBase) RegNoFoundHandle(handle NoFoundHandle) {
	rpc.exports.RegNoFoundHandle(handle)
}

func (rpc *rpcBase) RegProxy(id string, ptype reflect.Type) error {
	if ptype.ConvertibleTo(ProxyType) {
		return errors.New("Can not ConvertibleTo Proxy")
	}
	rpc.proxys[id] = ptype
	return nil
}

func (rpc *rpcBase) NewProxy(id string) (rs reflect.Value, err error) {
	t, ok := rpc.proxys[id]
	if !ok {
		//fmt.Errorf("Proxy(%s) no found", id)
		p := new(Proxyer)
		return reflect.ValueOf(p), nil
	}
	return reflect.New(t), nil
}

//
//
